
<a class="reference external image-reference" href="https://gitlab.com/benvial/emf2023/commits/main" target="_blank"><img alt="Release" src="https://img.shields.io/gitlab/pipeline/benvial/emf2023/main?logo=gitlab&labelColor=dedede&style=for-the-badge"></a> 


## Slides for EMF 2023
Cite as:
> B. Vial (2023, August 31), Topology optimization of electromagnetic devices: numerical implementation and applications

- [Web version](https://benvial.gitlab.io/emf2023)