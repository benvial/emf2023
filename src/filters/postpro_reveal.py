#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

import sys
from bs4 import BeautifulSoup


def main(fn):
    with open(fn, "r") as file:
        soup = BeautifulSoup(file, "html.parser")
    for item in soup.findAll(["section"]):
        if item.get("class") is not None:
            if "footnotes" in item.get("class"):
                item.name = "div"
    with open(fn, "w") as file:
        file.write(str(soup))



if __name__ == "__main__":
    main(sys.argv[1])
