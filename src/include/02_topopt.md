


# Topology optimization {data-background-color="var(--Imperial-Blue)"}

## Parametrization



- Material distribution in design domain $\Omega_{\rm des}$: density
function $p \in [0,1]$
- Each pixel in the mesh is a variable!

<img src="fig/p.png" style="width: 40%;" ></img>





### Filtering 

<!-- {data-background-color=black data-background-image="fig/photo-1602968407815-5963b28c66af.avif" data-background-opacity=0.4} -->

Avoid small features and \"chessboard\" pattern.

- Convolution: $f(\B r) = \frac{1}{A}{\rm exp}(-|\B r|^2 /R_f^2)$, with $\int_{\Omega_{\rm des}} f(\B r) =1$
\begin{equation*}
\densf(\B r)  = p * f = \int_{\Omega_{\rm des}} p(\B r') f(\B r -\B r') {\rm d} \B r'
\label{eq:gaussian_filt}
\end{equation*}
- PDE filter: solve a Helmholtz equation
$$-R_f^2 \B\nabla ^2 \densf + \densf = p {\quad\rm on \,}\Omega_{\rm des}, \grad\densf\cdotp\B n = 0 {\quad\rm on \,}\partial\Omega_{\rm des}$$

[@lazarovFiltersTopologyOptimization2011]

<!-- ### Filtering
<video data-autoplay src="fig/filter.mp4" style="width: 100%;"></video> -->

### Filtering
<img src="fig/filter.gif" style="width: 100%;" ></img>


## Projection

- Enforcing discrete design since intermediate densities are unphysical

$$\densp(\densf) = \frac{\tanh\left[\beta\nu\right] + \tanh\left[\beta(\densf-\nu)\right] }{\tanh\left[\beta\nu\right]
  + \tanh\left[\beta(1-\nu)\right]}$$
  with $\nu=1/2$ and $\beta>0$ increased during the course of the optimization.

- Can be extended to more levels

[@wangProjectionMethodsConvergence2010]
<!-- ### Projection
<video data-autoplay src="fig/proj.mp4" style="width: 100%;"></video> -->

### Projection
<img src="fig/proj.gif" style="width: 100%;" ></img>

### Interpolation
$\varepsilon(\densp)=(\varepsilon_{\rm max}-\varepsilon_{\rm min})\,\densp^m + \varepsilon_{\rm min}$


- Exponent $m=1$ in the following
- Can also interpolate the inverse/log etc. Might be better for high contrast.
[@bendsoeMaterialInterpolationSchemes1999]



::: notes

Exponent m: here it is linear in the following
Can also interpolate the inverse/log etc. Might be better for high contrast

:::


## Algorithm

<p style="margin-bottom:-2em;"></p>
::: columns

:::: {.column width=50% }


![](fig/topopt_algo.png){width=80%}

::::

:::: {.column width=50% }

<p style="margin-bottom:3cm;"></p>


- gradient based optimization algorithm: method of moving asymptotes (MMA[@svanbergClassGloballyConvergent]), free implementation via the `nlopt` package [@johnsonNLoptNonlinearoptimizationPackage]
- 40 iterations or until convergence on the objective or
design variables
-  repeated setting $\beta =2^n$, where $n$ is an integer between 0 and 7,
restarting the algorithm with the optimized density obtained at the
previous step

::::

:::